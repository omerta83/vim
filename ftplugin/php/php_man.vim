" put in ~/.vim/ftplugin/php_man.vim
" Will map K to show signature on commandline. 
" Will also attempt to show signature while typing.
"
" Set g:php_quick_man_color = 0 to disable color
" Set g:php_quick_man_insert_mode = 0 to disable insert mode signature

if exists('b:ftplugin_php_man')
    finish
endif
let b:ftplugin_php_man = 1

if !exists("b:undo_ftplugin")
  let b:undo_ftplugin = ""
endif

function! s:PHPQuickMan(word, ...)
  if !exists('g:php_builtin_functions')
    call phpcomplete#LoadData()
  endif
  let word = a:word . '('
  if !get(b:, 'php_quick_man_color', get(g:, 'php_quick_man_color', 1))
    if !a:0
      echo word . get(g:php_builtin_functions, word, ' can not be found')
    endif
  else
    let sig = get(g:php_builtin_functions, word, '')
    if sig == ''
      if !a:0
        echohl WarningMsg
        echo word . ' can not be found'
        echohl None
      endif
    else
      if a:0 && &showmode && &cmdheight <= 1
        let b:showmode = &showmode
        let &showmode = 0
      endif
      if a:0
        echo ''
      endif
      echon word
      for part in split(matchstr(sig, '.*\ze|\s\+\w\+$'), '\<')
        echohl Directory
        let w = matchstr(part, '\v^(int|array|mixed|callable|object|string|bool|void|resource)')
        echon w
        echohl None
        echon strpart(part, len(w))
      endfor
      echon ") | "
      echohl WarningMsg
      echon matchstr(sig, '|\s\+\zs\w\+$')
      echohl None
    endif
  endif
endfunction

nnoremap <buffer> K :call <SID>PHPQuickMan(<SID>seekWord(expand('<cword>')))<cr>
let b:undo_ftplugin = b:undo_ftplugin . "|nunmap <buffer> K"

augroup PHP_Man
  autocmd! * <buffer>
  autocmd CursorMovedI,InsertEnter <buffer> call <SID>PHPQuickManInsert()
  autocmd InsertLeave  <buffer> call <SID>restore()
augroup END

function! s:restore()
  if exists('b:showmode')
    let &showmode = b:showmode
    silent! unlet b:showmode
    echo ''
  endif
endfunction

function! s:seekWord(word)
  if !exists('g:php_builtin_functions')
    call phpcomplete#LoadData()
  endif
  if has_key(g:php_builtin_functions, a:word . '(', )
    return a:word
  endif
  let match = searchpairpos('(', '', ')', 'bncW', '', line('.'), 200)
  if match != [0, 0]
    let [lnum, col] = match
    let word = matchstr(getline(lnum)[0:col-1], '\<\k\+\ze($')
    return word
  endif
  return a:word
endfunction

function! s:PHPQuickManInsert()
  if !get(b:, 'php_quick_man_insert_mode', get(g:, 'php_quick_man_insert_mode', 1))
    return
  endif
  let word = s:seekWord('')
  if word != ''
    if word != 'array'
      call s:PHPQuickMan(word, 1)
    endif
  elseif get(b:, 'showmode', 0)
    call s:restore()
  endif
endfunction
