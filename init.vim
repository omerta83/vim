if has('mac')
    let g:python_host_prog='/usr/local/bin/python3'
else
    let g:python_host_prog='/usr/bin/python3'
endif
let g:mapleader = "\<Space>"
"let mapleader = ','
" Autoinstall vim-plug {{{
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall
endif
" }}}
call plug#begin('~/.config/nvim/plugged') " Plugins initialization start {{{
" }}}
" Appearance
" ====================================================================
Plug 'mhartington/oceanic-next'
" Plug 'ayu-theme/ayu-vim'
" Plug 'andreypopp/vim-colors-plain'
"Plug 'drewtempelmeyer/palenight.vim'
"Plug 'NLKNguyen/papercolor-theme'

" Completion
" ====================================================================
Plug 'Shougo/echodoc.vim'
" {{{
  let g:echodoc#enable_at_startup = 1
" }}}
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
" {{{
  let g:deoplete#enable_at_startup = 1
  " Use tab to move through completions
  inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"
  inoremap <expr><s-tab> pumvisible() ? "\<c-p>" : "\<s-tab>"
  " Close preview window automatically after selecting a completion
  autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif
  " let g:deoplete#omni#functions = {}
  " let g:deoplete#omni#functions.javascript = [
    " \ 'tern#Complete',
    " \ 'jspc#omni'
  " \]
  let g:deoplete#ignore_sources = get(g:, 'deoplete#ignore_sources', {})
  let g:deoplete#ignore_sources.php = ['phpcd', 'omni']
  let g:deoplete#sources = {}
  " let g:deoplete#sources.php = ['padawan', 'buffer']
  " let g:deoplete#sources.javascript = ['tern', 'jspc']
  let g:deoplete#sources.javascript = ['tern']
" }}}
" Plug 'padawan-php/deoplete-padawan', { 'do': 'composer install' }
Plug 'carlitux/deoplete-ternjs', { 'for': ['javascript', 'javascript.jsx'] }
Plug 'dNitro/vim-pug-complete', { 'for': ['jade', 'pug'] }
Plug 'lvht/phpcd.vim', { 'for': 'php', 'do': 'composer install' }
Plug 'mattn/emmet-vim'
" {{{
  let g:user_emmet_leader_key = '<C-Z>'
  " let g:user_emmet_install_global = 0
  " autocmd FileType html,css EmmetInstall
" }}}
"Plug 'mitsuse/autocomplete-swift'
"" {{{
"  " Jump to the first placeholder by typing `<C-k>`.
"  "autocmd FileType swift imap <buffer> <C-k> <Plug>(autocomplete_swift_jump_to_placeholder)
"  autocmd BufNewFile,BufRead *.swift set filetype=swift
"  autocmd BufNewFile,BufRead *.swift set makeprg=swift\ %
"" }}}
"Plug 'zchee/deoplete-go'
" Plug 'tobyS/vmustache'
" Plug 'tobyS/pdv'
" " {{{
"   let g:pdv_template_dir = $HOME . '/.config/nvim/plugged/pdv/templates_snip'
"   nnoremap <leader>ps :call pdv#DocumentCurrentLine()<CR>
"   " nnoremap <leader>ps :call pdv#DocumentWithSnip()<CR>
" " }}}

" File Navigation
" ====================================================================
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
" {{{
  let g:fzf_colors =
  \ { 'fg':      ['fg', 'Normal'],
    \ 'bg':      ['bg', 'Normal'],
    \ 'hl':      ['fg', 'Comment'],
    \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
    \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
    \ 'hl+':     ['fg', 'Statement'],
    \ 'info':    ['fg', 'PreProc'],
    \ 'border':  ['fg', 'Ignore'],
    \ 'prompt':  ['fg', 'Conditional'],
    \ 'pointer': ['fg', 'Exception'],
    \ 'marker':  ['fg', 'Keyword'],
    \ 'spinner': ['fg', 'Label'],
    \ 'header':  ['fg', 'Comment'] }
  let g:fzf_nvim_statusline = 0 " disable statusline overwriting

  nnoremap <silent> <leader><space> :Files<CR>
  nnoremap <silent> <leader>a :Buffers<CR>
  nnoremap <silent> <leader>A :Windows<CR>
  nnoremap <silent> <leader>; :BLines<CR>
  nnoremap <silent> <leader>o :BTags<CR>
  nnoremap <silent> <leader>O :Tags<CR>
  nnoremap <silent> <leader>? :History<CR>
  nnoremap <silent> <leader>/ :F<SPACE>
  nnoremap <silent> <leader>. :AgIn

  nnoremap <silent> <leader>gl :Commits<CR>
  nnoremap <silent> <leader>ga :BCommits<CR>
  nnoremap <silent> <leader>ft :Filetypes<CR>
  nnoremap <leader>ls :mark '<CR>:FindSymbols<CR>
  nnoremap <leader>lf :mark '<CR>:FindFunctions<CR>
  " nnoremap <leader>li :mark '<CR>:FindImpls<CR>

  imap <C-x><C-f> <plug>(fzf-complete-file-ag)
  imap <C-x><C-l> <plug>(fzf-complete-line)

  let g:rg_command = '
  \ rg --column --line-number --no-heading --fixed-strings --ignore-case --no-ignore --hidden --follow --color "always"
  \ -g "*.{js,json,php,md,styl,jade,html,phtml,css,config,py,xml,cpp,c,go,hs,rb,conf}"
  \ -g "!{.git,node_modules,pub,var,bin,setup}/*"
  \ -g "!{phpctags}" '

  command! -bang -nargs=* FindSymbols
  \ call fzf#vim#grep(
  \   'rg --column --line-number --no-heading --color=always "(private|public|protected)[ \t]+(static )*(\\\$[a-zA-Z0-9_]+)" -g "*.php" -S | rg -S '.shellescape(<q-args>), 1,
  \   <bang>0 ? fzf#vim#with_preview('up:60%')
  \           : fzf#vim#with_preview('right:50%:hidden', '?'),
  \   <bang>0)
  command! -bang -nargs=* FindFunctions
    \ call fzf#vim#grep(
    \   'rg --column --line-number --no-heading --color=always "function +([a-zA-Z0-9_]+)" -g "*.php" | rg -g "*.rs" -S '.substitute(shellescape(<q-args>), " ", "|rg -g '*.rs' -S", ""), 1,
    \   <bang>0 ? fzf#vim#with_preview('up:40%')
    \           : fzf#vim#with_preview('right:20%:hidden', '?'),
    \   <bang>0)
  " command! -bang -nargs=* FindImpls
    " \ call fzf#vim#grep(
    " \   'rg --column --line-number --no-heading --color=always "impl([ \t\n]*<[^>]*>)?[ \t]+(([a-zA-Z0-9_:]+)[ \t]*(<[^>]*>)?[ \t]+(for)[ \t]+)?([a-zA-Z0-9_]+)" | rg -S '.substitute(shellescape(<q-args>), " ", "|rg ", ""), 1,
    " \   <bang>0 ? fzf#vim#with_preview('up:60%')
    " \           : fzf#vim#with_preview('right:50%:hidden', '?'),
    " \   <bang>0)

  command! -bang -nargs=* F call fzf#vim#grep(g:rg_command .shellescape(<q-args>), 1, <bang>0)
  command -nargs=1 -complete=file E execute('silent! !mkdir -p "$(dirname "<args>")"') <Bar> e <args>
" }}}
" Plug 'scrooloose/nerdtree'
" {{{
  " let NERDTreeHijackNetrw=0
  " let g:NERDTreeWinSize=25
  " let g:NERDTreeAutoDeleteBuffer=1
  " let NERDTreeMinimalUI=1
  " let NERDTreeCascadeSingleChildDir=0
  " let g:NERDTreeDirArrowExpandable = '+'
  " let g:NERDTreeDirArrowCollapsible = '-'
" }}}
Plug 'tpope/vim-vinegar'
" {{{
  let g:netrw_list_hide = '\(^\|\s\s\)\zs\.\S\+'
" }}}
Plug 'Shougo/deol.nvim'

"" Efficient way to edit remote files in Vim
"Plug 'zenbro/mirror.vim'
" {{{
  "nnoremap <leader>mp :MirrorPush<CR>
  "nnoremap <leader>ml :MirrorPull<CR>
  "nnoremap <leader>md :MirrorDiff<CR>
  "nnoremap <leader>me :MirrorEdit<CR>
  "nnoremap <leader>mo :MirrorOpen<CR>
  "nnoremap <leader>ms :MirrorSSH<CR>
  "nnoremap <leader>mi :MirrorInfo<CR>
  "nnoremap <leader>mc :MirrorConfig<CR>
" }}}

" Text Manipulation
" ====================================================================
Plug 'tpope/vim-surround'
Plug 'scrooloose/nerdcommenter'
" {{{
  let g:NERDSpaceDelims = 1
" }}}
Plug 'Raimondi/delimitMate'
" {{{
  let delimitMate_expand_cr = 2
  let delimitMate_expand_space = 1 " {|} => { | }
" }}}
"Plug 'terryma/vim-multiple-cursors'

" Text Objects
" ====================================================================
Plug 'wellle/targets.vim'

" Languages
" ====================================================================
"Plug 'keith/Swift.vim'
"" Highlight enclosing tags
"Plug 'fatih/vim-go'
"" {{{
"  let g:go_highlight_build_constraints = 1
"  let g:go_highlight_extra_types = 1
"  let g:go_highlight_fields = 1
"  let g:go_highlight_functions = 1
"  let g:go_highlight_methods = 1
"  let g:go_highlight_operators = 1
"  let g:go_highlight_structs = 1
"  let g:go_highlight_types = 1
"  let g:go_auto_sameids = 1
"  let g:go_fmt_command = "goimports"
"" }}}
"Plug 'zchee/nvim-go', {'do': 'make'}
"Plug 'udalov/kotlin-vim'
Plug 'Valloric/MatchTagAlways'
Plug 'othree/html5.vim'
"" Yet Another JS Syntax
" Plug 'othree/yajs.vim', { 'for': 'javascript' }
Plug 'HerringtonDarkholme/yats.vim'
"Plug 'gavocanov/vim-js-indent'
Plug 'pangloss/vim-javascript'
Plug 'mxw/vim-jsx'
" {{{
  let g:jsx_ext_required = 1
" }}}
Plug 'othree/javascript-libraries-syntax.vim'
" {{{
  let g:used_javascript_libs = 'jquery,vue'
" }}}
Plug 'digitaltoad/vim-pug'
Plug 'hail2u/vim-css3-syntax'
Plug 'groenewege/vim-less'
Plug 'elzr/vim-json'
" Plug 'chrisbra/Colorizer'
" Plug 'tpope/vim-haml'
"Plug 'ap/vim-css-color'
" Plug 'jvirtanen/vim-octave'
" Plug 'vim-vdebug/vdebug'
Plug '2072/PHP-Indenting-for-VIm'
Plug 'StanAngeloff/php.vim'
" {{{
  let g:php_var_selector_is_identifier = 1
  " let g:php_sql_query = 0
  " let g:php_sql_heredoc = 0
  " let g:php_sql_nowdoc = 0
" }}}
Plug 'blueyed/smarty.vim'
" Plug 'iakio/smarty3.vim'
" " {{{
" au BufRead,BufNewFile *.tpl set filetype=smarty3
" " }}}
Plug 'moll/vim-node'
Plug 'jwalton512/vim-blade'
" {{{
  " Define some single Blade directives. This variable is used for highlighting only.
  let g:blade_custom_directives = ['datetime', 'javascript']

  " Define pairs of Blade directives. This variable is used for highlighting and indentation.
  let g:blade_custom_directives_pairs = {
      \   'markdown': 'endmarkdown',
      \   'cache': 'endcache',
      \ }
" }}}

" Git
" ====================================================================
" Plug 'tpope/vim-fugitive'
" " {{{
"  " Fix broken syntax highlight in gitcommit files
"  " (https://github.com/tpope/vim-git/issues/12)
"  " let g:fugitive_git_executable = 'LANG=en_US.UTF-8 git'
" 
"  nnoremap <silent> <leader>gs :Gstatus<CR>
"  " nnoremap <silent> <leader>gd :Gdiff<CR>
"  nnoremap <silent> <leader>gc :Gcommit<CR>
"  nnoremap <silent> <leader>gb :Gblame<CR>
"  nnoremap <silent> <leader>ge :Gedit<CR>
"  nnoremap <silent> <leader>gE :Gedit<space>
"  nnoremap <silent> <leader>gr :Gread<CR>
"  nnoremap <silent> <leader>gR :Gread<space>
"  nnoremap <silent> <leader>gw :Gwrite<CR>
"  nnoremap <silent> <leader>gW :Gwrite!<CR>
"  nnoremap <silent> <leader>gq :Gwq<CR>
"  nnoremap <silent> <leader>gQ :Gwq!<CR>
" 
"  function! ReviewLastCommit()
"    if exists('b:git_dir')
"      Gtabedit HEAD^{}
"      nnoremap <buffer> <silent> q :<C-U>bdelete<CR>
"    else
"      echo 'No git a git repository:' expand('%:p')
"    endif
"  endfunction
"  nnoremap <silent> <leader>g` :call ReviewLastCommit()<CR>
" 
"  augroup fugitiveSettings
"    autocmd!
"    autocmd FileType gitcommit setlocal nolist
"    autocmd BufReadPost fugitive://* setlocal bufhidden=delete
"  augroup END
" " }}}
" Plug 'junegunn/gv.vim'
"Plug 'airblade/vim-gitgutter'
"" {{{
" let g:gitgutter_map_keys = 0
" let g:gitgutter_max_signs = 2000
" let g:gitgutter_realtime = 1
" let g:gitgutter_eager = 1
" let g:gitgutter_sign_removed = '–'
" let g:gitgutter_diff_args = '--ignore-space-at-eol'
" nmap <silent> ]h :GitGutterNextHunk<CR>
" nmap <silent> [h :GitGutterPrevHunk<CR>
" nnoremap <silent> <Leader>gu :GitGutterRevertHunk<CR>
" nnoremap <silent> <Leader>gp :GitGutterPreviewHunk<CR><c-w>j
" nnoremap cog :GitGutterToggle<CR>
" nnoremap <Leader>gt :GitGutterAll<CR>
"" }}}

" Utility
" ====================================================================
"Plug 'neomake/neomake'
"" {{{
"  autocmd! BufWritePost * Neomake
"  let g:neomake_javascript_enabled_makers = ['eslint']
"" }}}
" Plug 'w0rp/ale'
" " {{{
  " let g:ale_sign_column_always = 1
  " let g:ale_sign_error = '⚑'
  " let g:ale_sign_warning = '⚐'
" " }}}
if !has('nvim')
    Plug 'roxma/vim-hug-neovim-rpc'
    Plug 'roxma/nvim-yarp'
endif
Plug 'sbdchd/neoformat'
Plug 'wincent/ferret'
Plug 'Yggdroot/indentLine'
" {{{
  " let g:indentLine_char = ''
  " let g:indentLine_first_char = ''
  let g:indentLine_char = '⎸'
  let g:indentLine_first_char = '⎸'
  let g:indentLine_showFirstIndentLevel = 1
  let g:indentLine_setColors = 0
" }}}
Plug 'mbbill/undotree'
" {{{
  set undofile
  " Auto create undodir if not exists
  let undodir = expand($HOME . '/.config/nvim/cache/undodir')
  if !isdirectory(undodir)
    call mkdir(undodir, 'p')
  endif
  let &undodir = undodir

  nnoremap <leader>U :UndotreeToggle<CR>
" }}}

" Misc
" ====================================================================
" Plug 'itchyny/calendar.vim', { 'on': 'Calendar' }
" " {{{
"   let g:calendar_date_month_name = 1
"   let g:calendar_google_calendar = 1
"   let g:calendar_google_task = 1
" " }}}
call plug#end() " Plugins initialization finished {{{
" }}}

" Plugin settings
" {{{ vim-json
let g:vim_json_syntax_conceal = 0
" let g:indentLine_noConcealCursor=""
" }}}
" {{{ ferret
nmap <leader>x <Plug>(FerretAck)
let g:FerretHlsearch = 1
" }}}

" General settings
" ===========================================================
filetype indent on
filetype plugin on
filetype plugin indent on

" Source .vimrc file if it is present in working directory
"set exrc
"set secure

"set tabstop=4
if has('nvim')
  set termguicolors
endif
"set tw=80
" Turn off text wrapping
set wrap linebreak
"set lazyredraw                      " Don't redraw screen during macros
" Indentation {{{
" ============================================================
set expandtab
set softtabstop=4
set shiftwidth=4
" }}}
1,$retab!
" Remove whitespace on save
" autocmd BufWritePre * %s/\s\+$//e
set autoindent
set smartindent
set smarttab
" Locate where folds are
set foldcolumn=1
set foldnestmax=2
set backspace=indent,eol,start
set nu " Line number on
" set relativenumber
set noswapfile
set incsearch " Incremental search
set nohlsearch " Turn off highlighting search matches
"set mouse=a " Mouse scroll on
set mouse=   " Disable mouse
set cinwords=if,else,while,do,for,switch,case
set wildmenu
set wildmode=longest:full,full
"set wildmode=full wildmenu  " Enable command-line tab completion
set wildignore+=*.a,*.o,*.obj,*.pyc,*.DS_Store,*.db,*.swp,*.tmp
" set completeopt=preview        " Don't show extra info on completion
set completeopt=menu        " Don't show extra info on completion
set ignorecase smartcase    " Only be case sensitive when search contains uppercase
set gdefault                " Assume /g flag on :s search
set hidden                  " Allow hidden buffers
set noshowcmd                 " Always show current command
" set regexpengine=2
set inccommand=nosplit
set shortmess=atIcF
set ruler
set nocursorline
"set enc=utf-8
set laststatus=2
set encoding=utf-8
set t_Co=256
set t_ut=
let &t_ut=''
" set sessionoptions=buffers,tabpages,folds
set viewoptions=cursor,folds,slash,unix
"" Save folds
au BufWinLeave * silent! mkview
au BufWinEnter * silent! loadview
" au BufWinLeave * mksession
" Set command-line history
set history=1000
"set viminfo='1000,%
set undolevels=1500          " Only undo up to 150 times
"set autoread " Stop popup when loading a modified file
" Set path for directories to search for with gf and find
set path+=**
"set viminfo='1000,%
"set viminfo^=!
set nobk nowb noswf         " Disable backup, swap
set report=0                " Always report when lines are changed
"set showcmd                 " Show incomplete command at bottom right
set scrolloff=5
set splitbelow              " Open new split windows below current
set splitright              " Open new split window right when go vertical"
set fo+=ql
set autoread
" Turn off auto window resizing
set noea
" Include $ in word list
au FileType php        setlocal isk+=$
" au FileType php        set keywordprg=pman
au FileType javascript setlocal isk+=$
" Use OS clipboard
set clipboard+=unnamed,unnamedplus
set vb

" Mappings
" Much easier to type commands this way
"no ; :
" Keep traditional ; functionality
"no \ ;
" Pressing v again brings you out of visual mode

" Show whitespace characters
set list
set listchars=tab:» 
" set listchars=tab:» ,eol:↓
" Default shell
" set sh=zsh
xno v <esc>

" GUI
" ==================================================
"
if has('gui_running')
    set go-=L " egmrLT
    set go-=r
    set go-=T
    if has('gui_gtk2')
        set guifont=Roboto\ Mono\ for\ Powerline\ h15
    elseif has('gui_macvim')
        set guifont=Roboto\ Mono\ for\ Powerline:h15
    endif
endif

" Colors and highlights
" ==================================================
" }}}
if has('statusline')

  function! ALEWarnings() abort
    let l:counts = ale#statusline#Count(bufnr(''))
    let l:all_errors = l:counts.error + l:counts.style_error
    let l:all_non_errors = l:counts.total - l:all_errors
    return l:counts.total == 0 ? '' : printf('  W:%d ', all_non_errors)
  endfunction

  function! ALEErrors() abort
    let l:counts = ale#statusline#Count(bufnr(''))
    let l:all_errors = l:counts.error + l:counts.style_error
    let l:all_non_errors = l:counts.total - l:all_errors
    return l:counts.total == 0 ? '' : printf(' E:%d ', all_errors)
  endfunction

  function! ALEStatus() abort
    let l:counts = ale#statusline#Count(bufnr(''))
    let l:all_errors = l:counts.error + l:counts.style_error
    let l:all_non_errors = l:counts.total - l:all_errors
    return l:counts.total == 0 ? ' √ ' : ''
  endfunction

  set laststatus=2
  set statusline=%<%f
  set statusline+=%w%h%m%r

  set statusline+=\ %y
  set statusline+=%=%-14.(%l,%c%V%)\ %p%%\ 

  " set statusline+=\%#StatusLineOk#%{ALEStatus()}
  " set statusline+=\%#StatusLineError#%{ALEErrors()}
  " set statusline+=\%#StatusLineWarning#%{ALEWarnings()}

endif

" let ayucolor="dark"   " for dark version of theme
" colorscheme ayu
colorscheme OceanicNext

" Current settings
hi vertsplit gui=none guifg=#424242 guibg=none ctermbg=none cterm=none ctermfg=none
hi LineNr ctermfg=237
hi StatusLine gui=underline guifg=#999999 guibg=none
hi StatusLineNC gui=underline guifg=#424242 guibg=none
hi StatusLineOk gui=underline guifg=#5FD7A7
hi StatusLineError gui=underline guifg=#fb007a
hi StatusLineWarning gui=underline guifg=#A89C14
hi Search ctermbg=58 ctermfg=15
hi Default ctermfg=1
hi clear SignColumn
hi SignColumn ctermbg=none
hi FoldColumn guibg=none ctermbg=none cterm=none
hi Folded guibg=none ctermbg=none cterm=none
" End current settings

" hi SignColumn ctermbg=235
" hi vertsplit ctermfg=238 ctermbg=235
" hi EndOfBuffer ctermfg=237 ctermbg=235
" hi CursorLineNR ctermbg=235

" Transparency
" hi GitGutterAdd ctermbg=none ctermfg=245
" hi GitGutterChange ctermbg=none ctermfg=245
" hi GitGutterDelete ctermbg=none ctermfg=245
" hi GitGutterChangeDelete ctermbg=none ctermfg=245
"hi LineNr ctermbg=none
"hi Normal ctermbg=none
"hi NonText ctermbg=none
"hi EndOfBuffer ctermbg=none
"hi CursorLineNR ctermbg=none

" set statusline=%=%f\ %m[L:%L]
"set fillchars=vert:\ ,stl:\ ,stlnc:\
" Hide INSERT in statusbar
set noshowmode

" Language-specific
" highlight! link elixirAtom rubySymbol
" }}}
" Key Mappings " {{{
nnoremap <leader>vi :tabedit $MYVIMRC<CR>

" Toggle quickfix
"map <silent> <F8> :copen<CR>

" Quick way to save file
nnoremap <leader>w :w<CR>

" Y behave like D or C
nnoremap Y y$

" Disable search highlighting
"nnoremap <silent> <Esc><Esc> :nohlsearch<CR><Esc>

" Copy current file path to clipboard
nnoremap <leader>% :call CopyCurrentFilePath()<CR>
function! CopyCurrentFilePath() " {{{
  let @+ = expand('%')
  echo @+
endfunction
" }}}

" Keep search results at the center of screen
nmap n nzz
nmap N Nzz
nmap * *zz
nmap # #zz
nmap g* g*zz
nmap g# g#zz

" Select all text
noremap vA ggVG

" Same as normal H/L behavior, but preserves scrolloff
nnoremap H :call JumpWithScrollOff('H')<CR>
nnoremap L :call JumpWithScrollOff('L')<CR>
function! JumpWithScrollOff(key) " {{{
  set scrolloff=0
  execute 'normal! ' . a:key
  set scrolloff=999
endfunction " }}}

" Switch between tabs
nmap <leader>1 1gt
nmap <leader>2 2gt
nmap <leader>3 3gt
nmap <leader>4 4gt
nmap <leader>5 5gt
nmap <leader>6 6gt
nmap <leader>7 7gt
nmap <leader>8 8gt
nmap <leader>9 9gt

" Creating splits with empty buffers in all directions
nnoremap <Leader>hn :leftabove  vnew<CR>
nnoremap <Leader>ln :rightbelow vnew<CR>
nnoremap <Leader>kn :leftabove  new<CR>
nnoremap <Leader>jn :rightbelow new<CR>

" If split in given direction exists - jump, else create new split
function! JumpOrOpenNewSplit(key, cmd, fzf) " {{{
  let current_window = winnr()
  execute 'wincmd' a:key
  if current_window == winnr()
    execute a:cmd
    if a:fzf
      Files
    endif
  else
    if a:fzf
      Files
    endif
  endif
endfunction " }}}
nnoremap <silent> <Leader>hh :call JumpOrOpenNewSplit('h', ':leftabove vsplit', 0)<CR>
nnoremap <silent> <Leader>ll :call JumpOrOpenNewSplit('l', ':rightbelow vsplit', 0)<CR>
nnoremap <silent> <Leader>kk :call JumpOrOpenNewSplit('k', ':leftabove split', 0)<CR>
nnoremap <silent> <Leader>jj :call JumpOrOpenNewSplit('j', ':rightbelow split', 0)<CR>

" Same as above, except it opens unite at the end
nnoremap <silent> <Leader>h<Space> :call JumpOrOpenNewSplit('h', ':leftabove vsplit', 1)<CR>
nnoremap <silent> <Leader>l<Space> :call JumpOrOpenNewSplit('l', ':rightbelow vsplit', 1)<CR>
nnoremap <silent> <Leader>k<Space> :call JumpOrOpenNewSplit('k', ':leftabove split', 1)<CR>
nnoremap <silent> <Leader>j<Space> :call JumpOrOpenNewSplit('j', ':rightbelow split', 1)<CR>

" Remove trailing whitespaces in current buffer
"nnoremap <Leader><BS>s :1,$s/[ ]*$//<CR>:nohlsearch<CR>1G

" Universal closing behavior
"nnoremap <silent> Q :call CloseSplitOrDeleteBuffer()<CR>
"nnoremap <silent> Й :call CloseSplitOrDeleteBuffer()<CR>
"function! CloseSplitOrDeleteBuffer() " {{{
"  if winnr('$') > 1
"    wincmd c
"  else
"    execute 'bdelete'
"  endif
"endfunction " }}}

" Delete all hidden buffers
"nnoremap <silent> <Leader><BS>b :call DeleteHiddenBuffers()<CR>
"function! DeleteHiddenBuffers() " {{{
"  let tpbl=[]
"  call map(range(1, tabpagenr('$')), 'extend(tpbl, tabpagebuflist(v:val))')
"  for buf in filter(range(1, bufnr('$')), 'bufexists(v:val) && index(tpbl, v:val)==-1')
"    silent execute 'bwipeout' buf
"  endfor
"endfunction " }}}

let g:session_dir = '$HOME/.config/nvim/sessions/'
nnoremap <Leader>sl :wall<Bar>execute "source " . g:session_dir . fnamemodify(getcwd(), ':t')<CR>
nnoremap <Leader>ss :execute "mksession! " . g:session_dir . fnamemodify(getcwd(), ':t')<CR>
" }}}
" Terminal {{{
" ====================================================================
nnoremap <silent> <leader><Enter> :tabnew<CR>:terminal<CR>

" Opening splits with terminal in all directions
if has('gui_running')
    nnoremap <Leader>h<Enter> :leftabove  :terminal<CR>
    nnoremap <Leader>l<Enter> :rightbelow :terminal<CR>
    nnoremap <Leader>k<Enter> :leftabove  :terminal<CR>
    nnoremap <Leader>j<Enter> :rightbelow :terminal<CR>
elseif has('nvim')
    nnoremap <Leader>h<Enter> :leftabove  vnew<CR>:terminal<CR>
    nnoremap <Leader>l<Enter> :rightbelow vnew<CR>:terminal<CR>
    nnoremap <Leader>k<Enter> :leftabove  new<CR>:terminal<CR>
    nnoremap <Leader>j<Enter> :rightbelow new<CR>:terminal<CR>
endif

" Open tig
nnoremap <Leader>gg :tabnew<CR>:terminal tig<CR>

if has('nvim')
  "tnoremap <C-[> <c-\><c-n><esc><cr>
  "tnoremap <F1> <C-\><C-n>
  tnoremap <C-\><C-\> <C-\><C-n>:bd!<CR>
endif

function! TerminalInSplit(args)
  botright split
  execute 'terminal' a:args
endfunction

nnoremap <leader><F1> :execute 'terminal ranger ' . expand('%:p:h')<CR>
nnoremap <leader><F2> :terminal ranger<CR>
augroup terminalSettings
  autocmd!
  autocmd FileType ruby nnoremap <leader>\ :call TerminalInSplit('pry')<CR>
augroup END
" }}}
" Autocommands {{{
" ====================================================================
" augroup vimGeneralCallbacks
  " autocmd!
  " "autocmd BufWritePost .nvimrc nested source ~/.config/nvim/init.vim
" augroup END

" Jump to last edit when opening Vim
au BufReadPost *
    \ if line("'\"") > 1 && line("'\"") <= line("$") && &ft !~# 'commit'
    \ | exe "normal! g`\""
    \ | endif

" augroup fileTypeSpecific
  " autocmd!
  " JST support
  " autocmd BufNewFile,BufRead *.ejs set filetype=jst
  " autocmd BufNewFile,BufRead *.jst set filetype=jst
  " autocmd BufNewFile,BufRead *.djs set filetype=jst
  " autocmd BufNewFile,BufRead *.hamljs set filetype=jst
  " autocmd BufNewFile,BufRead *.ect set filetype=jst

  " autocmd BufNewFile,BufRead *.js.erb set filetype=javascript

  " " Gnuplot support
  " autocmd BufNewFile,BufRead *.plt set filetype=gnuplot

  " autocmd FileType jst set syntax=htmldjango
  " au BufNewFile *.php 0r ~/.config/nvim/templates/php/skeleton.php
" augroup END

augroup quickFixSettings
  autocmd!
  autocmd FileType qf
        \ nnoremap <buffer> <silent> q :close<CR> |
        \ map <buffer> <silent> <F4> :close<CR> |
        \ map <buffer> <silent> <F8> :close<CR>
augroup END

if has('nvim')
  augroup terminalCallbacks
    autocmd!
    autocmd TermClose * call feedkeys('<cr>')
  augroup END
endif
"}}}
" Cursor configuration {{{
" ====================================================================
" Use a blinking upright bar cursor in Insert mode, a solid block in normal
" and a blinking underline in replace mode
" if has('nvim')
  "let $NVIM_TUI_ENABLE_CURSOR_SHAPE=1
  "let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  "set guicursor=n-v-c:block-Cursor/lCursor-blinkon0,i-ci:ver25-Cursor/lCursor,r-cr:hor20-Cursor/lCursor
  " set guicursor=n-v-c:block,i-ci-ve:ver25,r-cr:hor20,o:hor50
   " \,a:blinkwait700-blinkoff400-blinkon250-Cursor/lCursor
   " \,sm:block-blinkwait175-blinkoff150-blinkon175
" endif
if has('gui_running')
    " Disable cursor blinking in GUI
    let &guicursor = substitute(&guicursor, 'n-v-c:', '&blinkon0-', '')
endif

" Disable arrow keys
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>
" Easy window navigation
map <C-J> <C-W>j
map <C-K> <C-W>k
map <C-H> <C-W>h
map <C-L> <C-W>l
map <C-x>k :bdelete<CR>

" Remap number increment
" map <C-c> <C-A>

" Next/Previous buffer
" nmap <A-Left> :bp!<CR>
" nmap <A-Right> :bn!<CR>

" Go to begin/end of line in insert mode
inoremap <C-a> <Home>
inoremap <C-e> <End>

" Turn off highlighted search
nmap <silent> <Leader>nh :nohlsearch<CR>

" Disable auto-commenting
au FileType * setl fo-=cro

" Avoid vim highlighting curly braces in blocks as errors
let c_no_curly_error = 1
